var dictio = [];
dictio.active = 'cas';

var lang = [];
var labels_to_translate = ['l_smoking_condition', 'l_smoking_condition_non_smoker', 'l_smoking_condition_ex_smoker', 'l_smoking_condition_smoker', 'l_smoking_prochaska', 'l_smoking_prochaska_ready', 'l_smoking_prochaska_not_ready', 'l_smoking_dependence', 'l_smoking_dependence_high', 'l_smoking_dependence_low', 'l_diet_condition', 'l_diet_condition_adhered', 'l_diet_condition_not_adhered', 'l_diet_prochaska', 'l_diet_prochaska_ready', 'l_diet_prochaska_not_ready'];

lang['global'] = {'title' : 'Intervención',
                  'non_intervention': "No intervención",
                  'l_intervention_button': "Intervención recomendada",
                  'l_main_title': "Algoritmo multiriesgo",
                  'l_advice_error': "El formulario no está completo",
                  'l_clean_button' : "Limpiar formulario" };

lang['smoking'] = {'title' : 'Tabaco',
                 'l_smoking_condition' : "Hábito tabáquico",
                 'l_smoking_condition_non_smoker' : "No fumador",
                 'l_smoking_condition_ex_smoker' : "Ex fumador",
                 'l_smoking_condition_smoker' : "Fumador",
                 'l_smoking_prochaska' : "Motivación Prochaska",
                 'l_smoking_prochaska_ready' : "Sí preparado (en preparación o en acción)",
                 'l_smoking_prochaska_not_ready' : "No preparado (estado precontemplativo o contemplativo)",
                 'l_smoking_dependence' : "Grado de dependencia",
                 'l_smoking_dependence_high' : "Dependencia superior o igual a 3",
                 'l_smoking_dependence_low' : "Dependencia inferior a 3",
                 '0' : "No intervención. Práctica clínica habitual",
                 '1' : "No intervención. Práctica clínica habitual",
                 '3' : "Intervención motivacional para dejar de fumar",
                 '5' : "Intervención para dejar de fumar. Ofrecer ayuda conductual",
                 '6' : "Intervención para dejar de fumar. Ofrecer ayuda con fármacos"};

lang['diet'] = {'title' : 'Dieta',
                 'l_diet_condition': "Adherencia dieta mediteránea. 14 ítems-PREDIMED",
                 'l_diet_condition_adhered': "Sí adherencia (9 puntos o más)",
                 'l_diet_condition_not_adhered': "No adherencia (menos de 9 puntos)",
                 'l_diet_prochaska': "Motivación Prochaska",
                 'l_diet_prochaska_ready': "Sí preparado (en preparación o en acción)",
                 'l_diet_prochaska_not_ready': "No preparado (estado precontemplativo o contemplativo)",
                 '0' : "No intervención. Práctica clínica habitual",
                 '2' : "Intervención motivacional para mejorar adherencia a la dieta mediterránea",
                 '3' : "Intervención adherencia dieta mediterránea"};

lang['activity'] = {'title' : 'Actividad Física',
                 'l_activity_condition': "No sedentario:<br />30' AF moderada 5 o más días a la semana, o<br /> 3 o más sesiones a la semana de actividad intensa de almenos 30 minutos",
                 'l_activity_condition_active': "Activo",
                 'l_activity_condition_sedentary': "Sedentario",
                 'l_activity_prochaska': "Motivación Prochaska y Consejo personalizado sobre incremento de actividad física. Oferta de cita adicional",
                 'l_activity_prochaska_ready': "Sí preparado (en preparación o en acción)",
                 'l_activity_prochaska_not_ready': "No preparado (estado precontemplativo o contemplativo)",
                 '0' : "No intervención. Práctica clínica habitual",
                 '0' : "No intervención. Práctica clínica habitual",
                 '2' : "Intervención motivacional para incrementar actividad física. Material educativo",
                 '3' : "Intervención. Negociación actividad mínima inicial i progresión"};

lang['depression'] = {'title' : 'Depresión',
                    'l_depression_phq9' : "PHQ-9 (9+1 items) Patient Health Questionnaire",
                    'l_depression_phq9_negative' : "Paciente actualmente no deprimido",
                    'l_depression_phq9_positive' : "Paciente actualmente deprimido. Depresión mayor",
                    'l_depression_2quest_predict' : "Dos preguntas de cribado de depresión a lo largo de la vida",
                    'l_depression_2quest_predict_one_negative' : "No intervención PREDICT (alguna pregunta negativa)",
                    'l_depression_2quest_predict_all_positive' : "Si intervención PREDICT (2 preguntas positivas)",
                    'l_depression_predict_algorithm' : "Algoritmo de riesgo PredictD",
                    'l_depression_predict_algorithm_first_tercile' : "Riesgo bajo: primer tercil",
                    'l_depression_predict_algorithm_other_tercile' : "Riesgo moderado o alto: segundo y tercer tercilos",
                    '0' : "Paciente deprimido, tratamiento según guía clínica. No intervención predictD",
                    '2' : "Empoderar y activar al paciente, sugerir estrategias, actitudes y comportamientos que ya utilizen para prevenir la depresión",
                    '4' : "Intervención predictD telefónica o presencial (5 minutos)",
                    '5' : "Intervención predictD presencial (10 minutos)"};

lang['cv_risk'] = {'title' : 'Riesgo Cardiovascular',
                   'l_cv_risk_three_months': "Objetivo seguimiento a los 3 meses:<br />Regicor < 10% o Score < 5% y/o LDL < 130mg/dl (3.3mmol/L)",
                  'l_cv_risk_three_months_yes': 'Sí',
                  'l_cv_risk_three_months_no': 'No',
                  'l_follow_up_3_months': "Seguimiento a los 3 meses",
                  'l_cv_risk_antecedent' : "Antecendentes CV",
                  'l_cv_risk_antecedent_with' : "Sí",
                  'l_cv_risk_antecedent_without' : "No",
                  'l_cv_risk_coltot' : "Determinación Colesterol Total (CT)",
                  'l_cv_risk_coltot_low' : "CT < 200 mg/dl (5.2mmol/L)",
                  'l_cv_risk_coltot_high' : "CT &ge; 200 mg/dl (5.2mmol/L)",
                  'l_cv_risk_colldl' : "Perfil lípidico",
                  'l_cv_risk_colldl_low' : "LDL < 240 mg/dl (6mmol/L)",
                  'l_cv_risk_colldl_high' : "LDL &ge; 240 mg/dl (6mmol/L)",
                  'l_cv_risk_regicor_score': "Cálculo Riesgo CV (REGICOR, SCORE)",
                  'l_cv_risk_regicor_score_low': "REGICOR < 10% o SCORE < 5%",
                  'l_cv_risk_regicor_score_high': "REGICOR &ge; 10% o SCORE &ge; 5%",
                  'l_cv_risk_reason': "Cálculo REASON",
                  'l_cv_risk_reason_low': "REASON < 4.1",
                  'l_cv_risk_reason_high': "REASON &ge; 4.1",
                  'l_cv_risk_itb': "Medida ITB",
                  'l_cv_risk_itb_pathological': "ITB &le; 0.90",
                  'l_cv_risk_itb_regular': "ITB > 0.90",
                  '0' : "Prevención secundaria. No intervención cardiovascular. ",
                  '1' : "Seguimiento paciente según guía de práctica clínica del Centro de Salud. No intervención cardiovascular",
                  '4' : "Seguir consejos y medidas higiénico-dietéticas y de estilos de vida saludables. Seguimiento según la guía de práctica clínica del Centro de Salud",
                  '8' : "Seguir consejos y medidas higiénico-dietéticas y de estilos de vida saludables. Seguimiento a los 3 meses",
                  '10' : "Tratamiento con estatinas y consejos y medidas higiénico-dietéticas y de estilos de vida saludables. Seguimiento riesgo CV segun guía de práctica clínica del Centro de Salud",
                  '12' : "Modificación dosis estatinas y consejos y medidas higiénico-dietéticas y de estilos de vida saludables. Seguimiento riesgo CV segun guía de práctica clínica del Centro de Salud"};

dictio['cas'] = lang;

function get_text(field, sub){
  return dictio[dictio.active][field][sub];
}

function refresh_smoking(){
  if( $('#smoking_condition_smoker').prop('checked') ){
    $('#smoking_prochaska').show();
    if( $('#smoking_prochaska_ready').prop('checked') ){
      $('#smoking_dependence').show();
      if( $('#smoking_dependence_low').prop('checked') ) {
        return '5';
      }
      if( $('#smoking_dependence_high').prop('checked') ){
        return '6'; // High dependence
      }
      return '-1';
    }
    if( $('#smoking_prochaska_not_ready').prop('checked') ){
      $('#smoking_dependence').hide();
      return '3'; // Prochaska not ready
    }
    return '-1';
  }
  $('#smoking_prochaska').hide();
  $('#smoking_dependence').hide();
  if( $('#smoking_condition_non_smoker').prop('checked') ){
    return '0';
  }
  if( $('#smoking_condition_ex_smoker').prop('checked') ){
    return '1'; // Ex-smoker
  }
  return '-1';
}

function refresh_diet(){
  if( $('#diet_condition_not_adhered').prop('checked') ){
    $('#diet_prochaska').show();
    if( $('#diet_prochaska_ready').prop('checked') ){
      return '3'; // Prochaska ready
    }
    if( $('#diet_prochaska_not_ready').prop('checked') ){
      return '2'; // Prochaska not ready
    }
    return '-1';
  }
  $('#diet_prochaska').hide();
  if( $('#diet_condition_adhered').prop('checked') ){
    return '0';
  }
  return '-1';
}

function refresh_activity(){
  if( $('#activity_condition_sedentary').prop('checked') ){
    $('#activity_prochaska').show();
    if( $('#activity_prochaska_ready').prop('checked') ){
      return '3'; // Prochaska ready
    }
    if( $('#activity_prochaska_not_ready').prop('checked') ){
      return '2'; // Prochaska not ready
    }
    return '-1';
  }
  $('#activity_prochaska').hide();
  if( $('#activity_condition_active').prop('checked') ){
    return '0';
  }
  return '-1';
}

function refresh_depression(){
  if ( $('#depression_phq9_negative').prop('checked') ){
    $('#depression_2quest_predict').show();
    if ( $('#depression_2quest_predict_all_positive').prop('checked') ){
      $('#depression_predict_algorithm').show();
      if( $('#depression_predict_algorithm_first_tercile').prop('checked') ){
        return '4';
      }
      if( $('#depression_predict_algorithm_other_tercile').prop('checked') ){
        return '5';
      }
      return '-1';
    }
    if ( $('#depression_2quest_predict_one_negative').prop('checked') ){
      $('#depression_predict_algorithm').hide();
      return '2';
    }
    return '-1';
  }
  $('#depression_2quest_predict').hide();
  $('#depression_predict_algorithm').hide();
  if ( $('#depression_phq9_positive').prop('checked') ){
    return '0';
  }
  return '-1';
}

function refresh_cv_risk(){
  if( $('#cv_risk_antecedent_without').prop('checked') ){
    $('#cv_risk_coltot').show();
    if( $('#cv_risk_coltot_high').prop('checked') ){
      $('#cv_risk_colldl').show();
      if( $('#cv_risk_colldl_low').prop('checked') ){
        $('#cv_risk_regicor_score').show();
        if( $('#cv_risk_regicor_score_low').prop('checked') ){
          $('#cv_risk_reason').show();
          if( $('#cv_risk_reason_high').prop('checked') ){
            $('#cv_risk_itb').show();
            if( $('#cv_risk_itb_pathological').prop('checked') ){
              return '8';
            }
            if( $('#cv_risk_itb_regular').prop('checked') ){
              return '4';
            }
            return '-1';
          }
          if( $('#cv_risk_reason_low').prop('checked') ){
            $('#cv_risk_itb').hide();
            return '4';
          }
          return '-1';
        }
        if( $('#cv_risk_regicor_score_high').prop('checked') ){
          $('#cv_risk_reason').hide();
          $('#cv_risk_itb').hide();
          return '8';
        }
        return '-1';
      }
      if( $('#cv_risk_colldl_high').prop('checked') ){
        $('#cv_risk_regicor_score').hide();
        $('#cv_risk_reason').hide();
        $('#cv_risk_itb').hide();
        return '10';
      }
      return '-1';
    }
    if( $('#cv_risk_coltot_low').prop('checked') ){
      $('#cv_risk_colldl').hide();
      $('#cv_risk_regicor_score').hide();
      $('#cv_risk_reason').hide();
      $('#cv_risk_itb').hide();
      return '1';
    }
    return '-1';
  }
  $('#cv_risk_coltot').hide();
  $('#cv_risk_colldl').hide();
  $('#cv_risk_regicor_score').hide();
  $('#cv_risk_reason').hide();
  $('#cv_risk_itb').hide();
  if( $('#cv_risk_antecedent_with').prop('checked') ){
    return '0';
  }
  return '-1';
}

var state = [];
var intervention = [];

function health_intervention(state){
  var intervention = [];

  // S'ordena segons smoking->diet->activity
  if( ['0', '1'].indexOf(state['smoking']) < 0 ){
    intervention.push('smoking');
  }
  // Si intervenció estatina o seguiment a 3 mesos es posa al començament de tot
  if( state['cv_risk'] == '10'){
    intervention.push('cv_risk');
  }
  if( ['0'].indexOf(state['activity']) < 0 ){
    intervention.push('activity');
  }
  if( ['0'].indexOf(state['diet']) < 0 ){
    intervention.push('diet');
  }
  if( state['cv_risk'] == '8'){
    intervention.push('cv_risk');
  }
 // Si intervenció predictD es posa després d'estils de vida amb motivació
  if( ['4', '5'].indexOf(state['depression']) >= 0 )
    intervention.push('depression');

  // Es reordena si no hi ha motivació
  if( state['smoking'] == '3'){
    intervention.splice(intervention.indexOf('smoking'), 1);
    intervention.push('smoking');
  }
  if( state['activity'] == '2' ){
    intervention.splice(intervention.indexOf('activity'), 1);
    intervention.push('activity');
  }
  if( state['diet'] == '2' ){
    intervention.splice(intervention.indexOf('diet'), 1);
    intervention.push('diet');
  }


  if( intervention.indexOf('smoking') < 0 ){
    intervention.push('smoking');
  }
  if( intervention.indexOf('diet') < 0 ){
    intervention.push('diet');
  }
  if( intervention.indexOf('activity') < 0 ){
    intervention.push('activity');
  }
  if( intervention.indexOf('depression') < 0 ){
    intervention.push('depression');
  }
  if( intervention.indexOf('cv_risk') < 0 ){
    intervention.push('cv_risk');
  }

  return intervention;
}

function build_advice_error(state){
  $('#advice').empty();
  $('#advice').append("<br /><center><font size='3' color='red'>" +  get_text('global', 'l_advice_error') + "</font></center>")
}

function intervention_class(int, state){
  
  if(int == 'smoking'){
    console.log(['0', '1'].indexOf(state) );
    if( ['0', '1'].indexOf(state) >= 0 ) return 'no_intervention';
    if( ['5', '6'].indexOf(state) >= 0 ) return 'intervention';
    if(['3'].indexOf(state) >= 0 ) return 'no_motivated';
  }
  if(int == 'diet'){
    if( ['0'].indexOf(state) >= 0 ) return 'no_intervention';
    if( ['3'].indexOf(state) >= 0 ) return 'intervention';
    if( ['2'].indexOf(state) >= 0 ) return 'no_motivated';
  }
  if(int == 'activity'){
    if( ['0'].indexOf(state) >= 0 ) return 'no_intervention';
    if( ['3'].indexOf(state) >= 0 ) return 'intervention';
    if( ['2'].indexOf(state) >= 0 ) return 'no_motivated';
  }
  if(int == 'depression'){
    if( ['0', '2'].indexOf(state) >= 0 ) return 'no_intervention';
    if( ['4', '5'].indexOf(state) >= 0 ) return 'intervention';
    //if(! [].indexOf(state) < 0 ) return 'no_motivated';
  }
  if(int == 'cv_risk'){
    if( ['0', '1', '4'].indexOf(state) >= 0 ) return 'no_intervention';
    if( ['8', '10'].indexOf(state) >= 0 ) return 'intervention';
  }
}

function build_advice(intervention, state){
  $('#advice').empty();

  var txt = "<table id='advice_list'>";
  for(var i=0;i<intervention.length;i++){
    var int = intervention[i];
    txt += "<tr><td class='" + intervention_class(int, state[int]) + "'>" + get_text(int, 'title') + "</td><td  class='ui-widget-content'>" + get_text(int, state[int]) + "</td></tr>";
  }
  txt += '<br /></table>';
  $('#advice').append(txt);
}

function refreshUI(){
  var state = [];
  state['smoking'] = refresh_smoking();
  state['diet'] = refresh_diet();
  state['activity'] = refresh_activity();
  state['depression'] = refresh_depression();
  state['cv_risk'] = refresh_cv_risk();
  console.log("State:", state);
  if( state['smoking'] != '-1' & state['diet'] != '-1' & state['activity'] != '-1' & state['depression'] != '-1' & state['cv_risk'] != '-1'){
    var intervention = health_intervention(state);
    console.log("Intervention:", intervention);
    build_advice(intervention, state)
  }else{
    build_advice_error(state);
  }
}

function refreshFollowUp(){
  $('#cv_risk_three_months_output').empty();
  if( $('#cv_risk_three_months_yes').prop('checked') ){
    $('#cv_risk_three_months_output').append( '<b>' + get_text('cv_risk', '4') + '</b>' );
  }
  if( $('#cv_risk_three_months_no').prop('checked') ){
    $('#cv_risk_three_months_output').append( '<b>' + get_text('cv_risk', '10') + '</b>' );
  }
}

translate_labels = function(){
  labs = dictio[dictio.active]
  for(var i in labs){
    if (labs.hasOwnProperty(i)){
      for(var j in labs[i]){
        if (labs[i].hasOwnProperty(j) & j.substring(0, 2) == 'l_'){
          $('#' + j).html(get_text(i, j));
        }
      }
    }
  }
}

function uncheck_form(){
  $('#smoking_condition_non_smoker').prop('checked', false);
  $('#smoking_condition_ex_non_smoker').prop('checked', false);
  $('#smoking_condition_smoker').prop('checked', false);
  $('#smoking_prochaska_ready').prop('checked', false);
  $('#smoking_prochaska_not_ready').prop('checked', false);
  $('#smoking_dependence_high').prop('checked', false);
  $('#smoking_dependence_low').prop('checked', false);

  $('#diet_condition_adhered').prop('checked', false);
  $('#diet_condition_not_adhered').prop('checked', false);
  $('#diet_prochaska_ready').prop('checked', false);
  $('#diet_prochaska_not_ready').prop('checked', false);

  $('#activity_condition_active').prop('checked', false);
  $('#activity_condition_sedentary').prop('checked', false);
  $('#activity_prochaska_ready').prop('checked', false);
  $('#activity_prochaska_not_ready').prop('checked', false);

  $('#depression_phq9_negative').prop('checked', false);
  $('#depression_phq9_positive').prop('checked', false);
  $('#depression_2quest_predict_one_negative').prop('checked', false);
  $('#depression_2quest_predict_all_positive').prop('checked', false);
  $('#depression_predict_algorithm_first_tercile').prop('checked', false);
  $('#depression_predict_algorithm_other_tercile').prop('checked', false);

  $('#cv_risk_antecedent_with').prop('checked', false);
  $('#cv_risk_antecedent_without').prop('checked', false);
  $('#cv_risk_coltot_low').prop('checked', false);
  $('#cv_risk_coltot_high').prop('checked', false);
  $('#cv_risk_colldl_low').prop('checked', false);
  $('#cv_risk_colldl_high').prop('checked', false);
  $('#cv_risk_regicor_score_high').prop('checked', false);
  $('#cv_risk_regicor_score_low').prop('checked', false);
  $('#cv_risk_reason_low').prop('checked', false);
  $('#cv_risk_reason_high').prop('checked', false);
  $('#cv_risk_itb_pathological').prop('checked', false);
  $('#cv_risk_itb_regular').prop('checked', false);

  $('#cv_risk_three_months_yes').prop('checked', false);
  $('#cv_risk_three_months_no').prop('checked', false);
  $('#cv_risk_three_months_output').empty();

  $('#advice').dialog("close");
  $('#cv_risk_three_months').dialog("close");

  refreshUI();
}

function iniUI(){
  var tabs = ['smoking', 'diet', 'activity', 'depression', 'cv_risk'];
  for(var i=0;i<tabs.length;i++){
    $("#l_" + tabs[i]).html(get_text(tabs[i], 'title'));
  }

  translate_labels();

  $('#smoking_condition').change(refreshUI);
  $('#smoking_prochaska').change(refreshUI);
  $('#smoking_dependence').change(refreshUI);

  $('#diet_condition').change(refreshUI);
  $('#diet_prochaska').change(refreshUI);

  $('#activity_condition').change(refreshUI);
  $('#activity_prochaska').change(refreshUI);

  
  $('#depression_phq9').change(refreshUI);
  $('#depression_2quest_predict').change(refreshUI);
  $('#depression_predict_algorithm').change(refreshUI);

  
  $('#cv_risk_antecedent').change(refreshUI);
  $('#cv_risk_coltot').change(refreshUI);
  $('#cv_risk_colldl').change(refreshUI);
  $('#cv_risk_regicor_score').change(refreshUI);
  $('#cv_risk_reason').change(refreshUI);
  $('#cv_risk_itb').change(refreshUI);

  $('#cv_risk_three_months').change(refreshFollowUp);

  $('#advice').dialog({
      autoOpen: false,
      width: 600,
      position: {my: 'center top', at: 'right center', of: $('#smoking_condition') }
    });
  $('#l_intervention_button').click(function() {
    $( "#advice" ).dialog("open");
  });
  $('#cv_risk_three_months').dialog({
      autoOpen: false,
      width: 300,
      height:250,
      position: { my: 'right center', at: 'left center', of: $('#l_follow_up_3_months') }
    });
  $('#l_follow_up_3_months').click(function() {
    $( "#cv_risk_three_months" ).dialog("open");
  });
  $('#l_clean_button').click(function() {
    console.log("clicked");
    uncheck_form();
  });
  refreshUI();
}

$(document).ready(iniUI);
